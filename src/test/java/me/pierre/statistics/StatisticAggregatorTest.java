package me.pierre.statistics;

import me.pierre.model.AccessLogMessage;
import me.pierre.model.StatisticsMessage;
import org.junit.jupiter.api.Test;

import java.time.Instant;
import java.time.ZoneId;
import java.time.ZonedDateTime;

import static org.junit.jupiter.api.Assertions.*;

public class StatisticAggregatorTest {

  private static AccessLogMessage mkLogMsg(
      String ip, String user, String resource, int status, int size) {
    ZonedDateTime time = Instant.now().atZone(ZoneId.systemDefault());
    return new AccessLogMessage(ip, user, time, "", resource, "", status, size);
  }

  @Test
  public void shouldGenerateGlobalStatistics() {

    StatisticsAggregator statAgg = new StatisticsAggregator(null, 2, false);

    statAgg.handleAccessLogMessage(mkLogMsg("1.2.3.4", "user1", "/abc/aa", 200, 10));
    statAgg.handleAccessLogMessage(mkLogMsg("1.2.3.5", "user2", "/abd/aa/ef/", 201, 10));
    statAgg.handleAccessLogMessage(mkLogMsg("1.2.3.4", "user2", "/abc/aa/ef/gh", 300, 1));
    statAgg.handleAccessLogMessage(mkLogMsg("1.2.3.4", null, "/abd", 300, 1));
    statAgg.handleAccessLogMessage(mkLogMsg("1.2.3.4", null, "/abc/", 400, 1));

    StatisticsMessage statMsg = statAgg.aggregateStatistics();

    // We now test the global statistics
    assertNotNull(statMsg);
    assertEquals(statMsg.globalStats.hitCount, 5);
    assertEquals(statMsg.globalStats.hitSize, 23);
    assertEquals(statMsg.globalStats.status2xx, 2);
    assertEquals(statMsg.globalStats.status3xx, 2);
    assertEquals(statMsg.globalStats.status4xx, 1);
    assertEquals((int) statMsg.globalStats.distinctIpAddress.getEstimate(), 2);

    // We now test the statistics per user
    assertTrue(statMsg.users.containsKey("user1"));
    assertTrue(statMsg.users.containsKey("user2"));
    assertEquals(statMsg.users.size(), 2);

    assertEquals(statMsg.users.get("user1").hitCount, 1);
    assertEquals(statMsg.users.get("user1").status2xx, 1);
    assertEquals(statMsg.users.get("user1").status3xx, 0);

    assertEquals(statMsg.users.get("user2").hitCount, 2);
    assertEquals(statMsg.users.get("user2").status2xx, 1);
    assertEquals(statMsg.users.get("user2").status3xx, 1);

    // We now test the statistics per section
    assertTrue(statMsg.sections.containsKey("/abc"));
    assertTrue(statMsg.sections.containsKey("/abd"));
    assertEquals(statMsg.sections.size(), 2);

    assertEquals(statMsg.sections.get("/abc").hitCount, 3);
    assertEquals(statMsg.sections.get("/abc").status2xx, 1);
    assertEquals(statMsg.sections.get("/abc").status3xx, 1);
    assertEquals(statMsg.sections.get("/abc").status4xx, 1);

    assertEquals(statMsg.sections.get("/abd").hitCount, 1);
    assertEquals(statMsg.sections.get("/abd").status2xx, 1);
    assertEquals(statMsg.sections.get("/abd").status3xx, 0);
  }
}
