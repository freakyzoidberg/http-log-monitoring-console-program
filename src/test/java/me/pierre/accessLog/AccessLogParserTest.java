package me.pierre.accessLog;

import me.pierre.model.AccessLogMessage;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class AccessLogParserTest {
  AccessLogParser parser = new AccessLogParser();

  @Test
  public void shouldDecodeStandardHTTPLog() {
    String log =
        "45.101.194.6 - james [03/Mar/2020:23:13:08 -0700] \"GET /apache_pb.gig HTTP/1.0\" 202 9095";
    AccessLogMessage msg = parser.parse(log);
    assertEquals(msg.ipAddress, "45.101.194.6");
    assertEquals(msg.user, "james");
    assertEquals(msg.status, 202);
    assertEquals(msg.size, 9095);
    assertEquals(msg.resource, "/apache_pb.gig");
  }

  @Test
  public void shouldDecodeStandardHTTPLogWithMissing() {
    String log =
        "45.101.194.6 - - [03/Mar/2020:23:13:08 -0700] \"GET /apache_pb.gig HTTP/1.0\" - -";
    AccessLogMessage msg = parser.parse(log);
    assertEquals(msg.ipAddress, "45.101.194.6");
    assertEquals(msg.user, null);
    assertEquals(msg.status, 0);
    assertEquals(msg.size, 0);
    assertEquals(msg.resource, "/apache_pb.gig");
  }

  @Test
  public void shouldNotDecodeInvalidHTTPLog() {
    String log = "45.101.194.6 - - \"GET /apache_pb.gig HTTP/1.0\" - -";
    ;
    Exception exception =
        assertThrows(
            IllegalArgumentException.class,
            () -> {
              parser.parse(log);
            });
  }
}
