package me.pierre.alerts;

import com.google.common.eventbus.EventBus;
import com.google.common.eventbus.Subscribe;
import me.pierre.model.AlertMessage;
import me.pierre.model.StatisticsMessage;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class AlertsManagerTest {
  private static StatisticsMessage mkStatMsg(int hitCount) {
    StatisticsMessage stat = new StatisticsMessage();
    stat.globalStats.hitCount = hitCount;
    return stat;
  }

  private static void assertNoAlert(AlertMessage msg, int alertHitRatePerSec) {
    assertEquals(msg.status, AlertMessage.Status.NOALERT);
    assertTrue(msg.hitsPerSec < alertHitRatePerSec);
  }

  @Test
  public void shouldNotDecodeInvalidHTTPLog() throws InterruptedException {
    EventBus eventbus = new EventBus();
    final AlertMessage[] alertMessage = {null};

    final int alertHitRatePerSec = 5;

    AlertsManager alertMgr = new AlertsManager(eventbus, alertHitRatePerSec, 4, 2);

    eventbus.register(alertMgr);
    eventbus.register(
        new Object() {
          @Subscribe
          public void handleAlert(AlertMessage msg) {
            alertMessage[0] = msg;
          }
        });

    eventbus.post(mkStatMsg(10));
    // our current hit rate is 10 / 4sec == 2.5 which is bellow the 5 threshold, no alert
    assertNull(alertMessage[0]);

    eventbus.post(mkStatMsg(9));
    // our current hit rate is (10 + 9) / 4sec == 4.75 which is bellow the 5 threshold, no alert
    assertNull(alertMessage[0]);

    eventbus.post(mkStatMsg(11));
    // our current hit rate is (9 + 11) / 4sec == 5 which is equal to the 5 threshold, should have
    // an alert
    AlertMessage firstAlert = alertMessage[0];
    assertNotNull(firstAlert);
    assertEquals(firstAlert.status, AlertMessage.Status.ALERT);
    assertTrue(firstAlert.hitsPerSec >= alertHitRatePerSec);
    assertEquals(firstAlert.hitsPerSec, 5);
    // Make sure the timestamp of the alert is constant
    String alertMsg1 = firstAlert.toHumanString();
    Thread.sleep(1200);
    assertEquals(alertMsg1, firstAlert.toHumanString());

    eventbus.post(mkStatMsg(9));
    // our current hit rate is (11 + 9) / 4 sec == 5 which is equal to the 5 threshold, should still
    // have the same alert
    assertEquals(alertMessage[0].status, AlertMessage.Status.ALERT);
    assertTrue(alertMessage[0].hitsPerSec >= alertHitRatePerSec);
    assertSame(firstAlert, alertMessage[0]);

    eventbus.post(mkStatMsg(1));
    // our current hit rate is (9 + 1) / 4 sec == 2.5 which is less than the 5 threshold, alert
    // should recover
    assertNoAlert(alertMessage[0], alertHitRatePerSec);
    assertEquals(alertMessage[0].hitsPerSec, (int) 2.5);

    eventbus.post(mkStatMsg(1));
    // our current hit rate is (1 + 1) / 4 sec == 0.5 which is less than the 5 threshold, still no
    // alert
    assertNoAlert(alertMessage[0], alertHitRatePerSec);

    eventbus.post(mkStatMsg(10));
    // our current hit rate is (1 + 10) / 4 sec == 2.75 which is less than the 5 threshold, still no
    // alert
    assertNoAlert(alertMessage[0], alertHitRatePerSec);

    eventbus.post(mkStatMsg(30));
    // our current hit rate is (10 + 30) / 4 sec == 10 which is more than the 5 threshold, should
    // have a new alert
    AlertMessage secondAlert = alertMessage[0];
    assertEquals(secondAlert.status, AlertMessage.Status.ALERT);
    assertTrue(secondAlert.hitsPerSec >= alertHitRatePerSec);
    assertEquals(secondAlert.hitsPerSec, 10);
    assertNotSame(firstAlert, secondAlert);
  }
}
