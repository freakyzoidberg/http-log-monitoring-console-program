package me.pierre;

import com.google.common.eventbus.EventBus;
import me.pierre.accessLog.AccessLogProducer;
import me.pierre.alerts.AlertsManager;
import me.pierre.gui.ConsoleGUI;
import me.pierre.gui.ConsoleLOG;
import me.pierre.statistics.StatisticsAggregator;
import org.apache.commons.cli.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;

public class App {
  private static final Logger logger = LoggerFactory.getLogger(App.class);

  public static void main(String[] args) {
    AppOptions options = null;
    try {
      options = parseArguments(args);
      validateArguments(options);
      logger.info("Initializing the application with the given options {}", options);
    } catch (ParseException | IllegalArgumentException e) {
      logger.error(e.getMessage(), e);
      showUsage();
      System.exit(1);
    }

    // Create a bus so we can attach our components.
    EventBus eventBus = new EventBus();

    // Start the GUI
    startGUI(eventBus, options.consoleOnly);

    // Add a StatisticsAggregator and an AlertManager instance onto the bus
    eventBus.register(new StatisticsAggregator(eventBus, options.statIntervalSec));
    eventBus.register(
        new AlertsManager(
            eventBus, options.alertThresholdBySec, options.alertSpanSec, options.statIntervalSec));

    // Initialise the AccessLog producer
    new AccessLogProducer(eventBus, options.httpAccessLog);
  }

  /**
   * @param eventBus the bus where the statistics and alerts will be found
   * @param consoleOnly if we should rely on the console and emit log line or start our own GUI
   *     (ncurse-like)
   */
  private static void startGUI(EventBus eventBus, boolean consoleOnly) {
    if (consoleOnly) {
      ConsoleLOG gui = new ConsoleLOG();
      eventBus.register(gui);
    } else {
      // For the GUI we will need a dedicated thread.
      new Thread(
              () -> {
                ConsoleGUI gui = new ConsoleGUI();
                eventBus.register(gui);
                try {
                  gui.start(() -> System.exit(0));
                } catch (IOException e) {
                  System.exit(1);
                }
              },
              "gui-thread")
          .start();
    }
  }

  /**
   * Parse application arguments
   *
   * @param args the arguments to parse
   * @return an {@link AppOptions} with arguments value or their default;
   * @throws ParseException if the arguments cannot be parsed
   */
  private static AppOptions parseArguments(String[] args) throws ParseException {
    AppOptions options = new AppOptions();
    CommandLineParser parser = new DefaultParser();
    CommandLine commandLine = parser.parse(optionParameters(), args);

    if (commandLine.hasOption("help")) {
      showUsage();
      System.exit(0);
    }

    String file = commandLine.getOptionValue("file-access-log");
    if (file != null) {
      options.httpAccessLog = file;
    }

    String statInterval = commandLine.getOptionValue("stat-interval");
    if (statInterval != null) {
      options.statIntervalSec = Integer.parseInt(statInterval);
    }

    String alertSpan = commandLine.getOptionValue("alert-span");
    if (alertSpan != null) {
      options.alertSpanSec = Integer.parseInt(alertSpan);
    }

    String alertThreshold = commandLine.getOptionValue("alert-threshold");
    if (alertThreshold != null) {
      options.alertThresholdBySec = Integer.parseInt(alertThreshold);
    }

    options.consoleOnly = commandLine.hasOption("console");

    return options;
  }

  /**
   * Validate option arguments
   * @param options parsed arguments to run with
   */
  private static void validateArguments(AppOptions options) {

    if (options.statIntervalSec > options.alertSpanSec) {
      throw new IllegalArgumentException("Alert span must be larger than stat interval duration");
    }

    if (options.alertSpanSec % options.statIntervalSec != 0) {
      throw new IllegalArgumentException("Alert span must be a factor of stat interval duration");
    }
  }

  /**
   * Application options definitions
   *
   * @return an Options definitions defining the available application parameters
   */
  private static Options optionParameters() {
    AppOptions defaultOptions = new AppOptions();

    final Option httpAccesslogOption =
        Option.builder("f")
            .longOpt("file-access-log") //
            .desc("HTTP log file (default: " + defaultOptions.httpAccessLog + ")")
            .hasArg(true)
            .required(false)
            .build();

    final Option statIntervalOption =
        Option.builder("s")
            .longOpt("stat-interval") //
            .desc("Statistics interval (default: " + defaultOptions.statIntervalSec + " (sec))")
            .hasArg(true)
            .required(false)
            .build();

    final Option alertSpanOption =
        Option.builder("a")
            .longOpt("alert-span") //
            .desc("Alerts span (default: " + defaultOptions.alertSpanSec + " (sec))")
            .hasArg(true)
            .required(false)
            .build();

    final Option alertThresholdOption =
        Option.builder("t")
            .longOpt("alert-threshold") //
            .desc("Alerts threshold (default: " + defaultOptions.alertThresholdBySec + " (/sec))")
            .hasArg(true)
            .required(false)
            .build();

    final Option consoleOption =
        Option.builder("c")
            .longOpt("console") //
            .desc("Disable GUI - use console instead")
            .hasArg(false)
            .required(false)
            .build();

    final Option helpOption = Option.builder("h").longOpt("help").desc("Show this usage").build();

    final Options options = new Options();

    options.addOption(httpAccesslogOption);
    options.addOption(statIntervalOption);
    options.addOption(alertSpanOption);
    options.addOption(alertThresholdOption);
    options.addOption(helpOption);
    options.addOption(consoleOption);
    return options;
  }

  /** Print the application uage to standard out */
  private static void showUsage() {
    final HelpFormatter formatter = new HelpFormatter();
    formatter.printHelp("./httpMonitor.sh", optionParameters(), true);
  }

  /** Data class defining the application parameters default */
  private static class AppOptions {
    String httpAccessLog = "/tmp/access.log";
    int statIntervalSec = 10;
    int alertSpanSec = 120;
    int alertThresholdBySec = 10;
    boolean consoleOnly = false;
  }
}
