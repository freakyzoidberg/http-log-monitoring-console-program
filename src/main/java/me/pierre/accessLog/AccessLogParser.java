package me.pierre.accessLog;

import me.pierre.model.AccessLogMessage;

import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/** Utility class to parse W3C http log message into {@link AccessLogMessage} */
public class AccessLogParser {
  // Regex pattern for the W3C http log format
  private static final Pattern LOG_PATTERN = Pattern.compile(accessLogRegex());
  // Regex pattern for parsing datetime as present in the http log
  private static final DateTimeFormatter DATETIME_PATTERN =
      DateTimeFormatter.ofPattern("dd/MMM/yyyy:HH:mm:ss Z");

  /**
   * Construct a regex string for the W3C http log pattern
   *
   * @return a regex string to be used by Pattern
   */
  private static String accessLogRegex() {
    return new StringBuilder()
        .append("^(?<ipAddress>\\S+)")
        .append(" (?<identd>\\S+)")
        .append(" (?<user>\\S+)")
        .append(" \\[(?<datetime>[\\w:/]+\\s[+\\-]\\d{4})\\]")
        .append(" \"(?<verb>\\S+)\\s?(?<resource>\\S+)?\\s?(?<protocol>\\S+)?\"")
        .append(" (?<status>\\d{3}|-)")
        .append(" (?<size>\\d+|-)")
        .toString();
  }

  /**
   * Parse a w3c http log datetime into a ZonedDateTime
   *
   * @param datetime string in the w3c log format
   * @return a timezone aware datetime
   */
  private static ZonedDateTime parseDateTime(String datetime) {
    return ZonedDateTime.parse(datetime, DATETIME_PATTERN);
  }

  /**
   * Parse a number from a w3c http log
   *
   * @param value number or "-" if field is missing
   * @return the int value or 0 if field was missing
   */
  private static int parseNumber(String value) {
    if (value.equals("-")) {
      return 0;
    }
    return Integer.parseInt(value);
  }

  /**
   * Parse a string variable from a w3c http log
   *
   * @param value string or "-" if the field is missing
   * @return the string variable or null if the field was missing
   */
  private static String parseOptionalString(String value) {
    if (value.equals("-")) {
      return null;
    }
    return value;
  }

  /**
   * Parse a w3c http log into an {@link AccessLogMessage}
   *
   * @param line from a w3c http log
   * @return an {@link AccessLogMessage} with value parsed
   */
  public AccessLogMessage parse(String line) {
    Matcher matcher = LOG_PATTERN.matcher(line);
    if (matcher.find()) {
      return new AccessLogMessage(
          matcher.group("ipAddress"),
          parseOptionalString(matcher.group("user")),
          parseDateTime(matcher.group("datetime")),
          matcher.group("verb"),
          matcher.group("resource"),
          matcher.group("protocol"),
          parseNumber(matcher.group("status")),
          parseNumber(matcher.group("size")));
    }
    throw new IllegalArgumentException("Failed to parse log line: " + line);
  }
}
