package me.pierre.accessLog;

import com.google.common.eventbus.EventBus;
import org.apache.commons.io.input.Tailer;
import org.apache.commons.io.input.TailerListenerAdapter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;

/**
 * Producer class for AccessLogMessage it will emit {@link me.pierre.model.AccessLogMessage} on the
 * bus it is responsible for watching and tailing the http log
 */
public class AccessLogProducer extends TailerListenerAdapter {
  private static final Logger logger = LoggerFactory.getLogger(AccessLogProducer.class);

  private final AccessLogParser accessLogParser = new AccessLogParser();
  private final EventBus eventBus;

  /**
   * Watch and tail the give http log file location. This is blocking.
   *
   * @param eventBus the bus where we should emit the {@link me.pierre.model.AccessLogMessage}
   * @param fileAccessLog the location of the w3c http log
   */
  public AccessLogProducer(EventBus eventBus, String fileAccessLog) {
    this.eventBus = eventBus;
    Tailer tailer = new Tailer(new File(fileAccessLog), this, 200, true);
    tailer.run();
  }

  /**
   * For every line in the log we parse it and add the corresponding {@link
   * me.pierre.model.AccessLogMessage} to the bus
   *
   * @param line tailed from the w3c log file
   */
  @Override
  public void handle(String line) {
    try {
      eventBus.post(accessLogParser.parse(line));
    } catch (IllegalArgumentException e) {
      logger.warn("Failed to parse log line, ignoring...", e);
    }
  }

  /** If the file is missing or is removed and we cannot find a new one we log the information. */
  @Override
  public void fileNotFound() {
    logger.warn("File not found, retrying...");
  }
}
