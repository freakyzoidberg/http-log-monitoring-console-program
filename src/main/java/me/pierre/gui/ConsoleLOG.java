package me.pierre.gui;

import com.google.common.eventbus.Subscribe;
import me.pierre.model.AlertMessage;
import me.pierre.model.StatisticsMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/** Simple EventBus Consumer to emit to console the statistics and alerts state. */
public class ConsoleLOG {
  private static final Logger logger = LoggerFactory.getLogger(ConsoleLOG.class);

  @Subscribe
  public void handleStatisticsMessage(StatisticsMessage msg) {
    logger.info("{}", msg);
  }

  @Subscribe
  public void handleAlertMessage(AlertMessage msg) {
    logger.warn("{}", msg.toHumanString());
  }
}
