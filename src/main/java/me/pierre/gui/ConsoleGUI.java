package me.pierre.gui;

import com.google.common.eventbus.Subscribe;
import com.googlecode.lanterna.TextColor;
import com.googlecode.lanterna.gui2.DefaultWindowManager;
import com.googlecode.lanterna.gui2.EmptySpace;
import com.googlecode.lanterna.gui2.MultiWindowTextGUI;
import com.googlecode.lanterna.screen.Screen;
import com.googlecode.lanterna.screen.TerminalScreen;
import com.googlecode.lanterna.terminal.DefaultTerminalFactory;
import com.googlecode.lanterna.terminal.Terminal;
import me.pierre.model.AlertMessage;
import me.pierre.model.StatisticsMessage;

import java.io.IOException;

/**
 * Entry point for NCurse like GUI
 */
public class ConsoleGUI {
  private final ConsoleGUIWindow window;

  public ConsoleGUI() {
    this.window = new ConsoleGUIWindow();
  }

  public void start(Runnable callback) throws IOException {
    // Setup terminal and screen layers
    Terminal terminal = new DefaultTerminalFactory().createTerminal();

    try (Screen screen = new TerminalScreen(terminal)) {
      screen.setCursorPosition(null);
      screen.startScreen();
      MultiWindowTextGUI gui =
          new MultiWindowTextGUI(
              screen, new DefaultWindowManager(), new EmptySpace(TextColor.ANSI.BLACK));
      gui.addWindowAndWait(window);
      callback.run();
    }
  }

  @Subscribe
  public void handleStatisticsMessage(StatisticsMessage msg) {
    this.window.refreshStatistics(msg);
  }

  @Subscribe
  public void handleAlertsMessage(AlertMessage msg) {
    this.window.refreshAlerts(msg);
  }
}
