package me.pierre.gui;

import com.googlecode.lanterna.TextColor;
import com.googlecode.lanterna.gui2.*;
import me.pierre.model.AlertMessage;
import me.pierre.model.StatisticsMessage;
import me.pierre.statistics.StatisticsUtil;

import java.text.MessageFormat;
import java.util.Arrays;
import java.util.Deque;
import java.util.LinkedList;
import java.util.Map;

public class ConsoleGUIWindow extends BasicWindow {
  // buffer of recent Alert messages
  private final Deque<AlertMessage> alertMessages = new LinkedList<>();

  // Panels for rendenering
  private final Panel globalStats = new Panel(new GridLayout(12));
  private final Panel bySectionStats = new Panel(new LinearLayout());
  private final Panel byUserStats = new Panel(new LinearLayout());
  private final Panel alerts = new Panel(new LinearLayout());

  // Layout utilities for columns width;
  private static final LayoutData thirdCol =
      GridLayout.createLayoutData(
          GridLayout.Alignment.BEGINNING, GridLayout.Alignment.BEGINNING, true, false, 4, 1);
  private static final LayoutData quarterCol =
      GridLayout.createLayoutData(
          GridLayout.Alignment.BEGINNING, GridLayout.Alignment.BEGINNING, true, false, 3, 1);

  /** Default class constructor. */
  ConsoleGUIWindow() {

    Panel topPanel = new Panel(new GridLayout(12));
    topPanel.addComponent(
        globalStats.withBorder(Borders.singleLine("Global Statistics")),
        GridLayout.createLayoutData(
            GridLayout.Alignment.FILL, GridLayout.Alignment.BEGINNING, true, false, 12, 2));
    topPanel.addComponent(
        alerts,
        GridLayout.createLayoutData(
            GridLayout.Alignment.FILL, GridLayout.Alignment.BEGINNING, true, false, 12, 2));
    topPanel.addComponent(
        bySectionStats.withBorder(Borders.singleLine("By Section Statistics")),
        GridLayout.createLayoutData(
            GridLayout.Alignment.FILL, GridLayout.Alignment.FILL, true, true, 6, 4));
    topPanel.addComponent(
        byUserStats.withBorder(Borders.singleLine("By User Statistics")),
        GridLayout.createLayoutData(
            GridLayout.Alignment.FILL, GridLayout.Alignment.FILL, true, true, 6, 4));

    setComponent(topPanel);
    setHints(Arrays.asList(Hint.FULL_SCREEN, Hint.NO_DECORATIONS));

    globalStats.addComponent(new Label("Waiting for statistics..."));
  }

  /**
   * handler to refresh all panel using {@link StatisticsMessage}
   * @param msg {@link StatisticsMessage} to display
   */
  public void refreshStatistics(StatisticsMessage msg) {
    refreshGlobalStats(msg);
    refreshGroupsStats(bySectionStats, msg.sections);
    refreshGroupsStats(byUserStats, msg.users);
  }

  /**
   * Handler to update the global stats panel
   * @param msg {@link StatisticsMessage} to display
   */
  private void refreshGlobalStats(StatisticsMessage msg) {
    Panel pnl = globalStats;
    StatisticsMessage.Statistic stat = msg.globalStats;

    pnl.removeAllComponents();

    pnl.addComponent(new Label(MessageFormat.format("Requests: {0}", stat.hitCount)), thirdCol);
    pnl.addComponent(new Label(MessageFormat.format("Bytes: {0}", stat.hitSize)), thirdCol);
    pnl.addComponent(
        new Label(
            MessageFormat.format("Distinct IPs: {0}", (int) stat.distinctIpAddress.getEstimate())),
        thirdCol);
    pnl.addComponent(new Label(MessageFormat.format("2xx: {0}", stat.status2xx)), quarterCol);
    pnl.addComponent(new Label(MessageFormat.format("3xx: {0}", stat.status3xx)), quarterCol);
    pnl.addComponent(new Label(MessageFormat.format("4xx: {0}", stat.status4xx)), quarterCol);
    pnl.addComponent(new Label(MessageFormat.format("5xx: {0}", stat.status5xx)), quarterCol);
  }

  /**
   * Handler to update the by User or by Section panel
   * @param pnl panel to update
   * @param groups map of Statistics for the panel
   */
  private void refreshGroupsStats(Panel pnl, Map<String, StatisticsMessage.Statistic> groups) {
    pnl.removeAllComponents();
    Map<String, StatisticsMessage.Statistic> topSections =
        StatisticsUtil.getTopNHits(groups, 3, StatisticsUtil::compareStatisticByHit);

    int index = 0;
    for (Map.Entry<String, StatisticsMessage.Statistic> entry : topSections.entrySet()) {
      if (index > 0) pnl.addComponent(new EmptySpace());
      StatisticsMessage.Statistic stat = entry.getValue();
      pnl.addComponent(new Label(entry.getKey()));
      pnl.addComponent(new Label(MessageFormat.format("Requests: {0}", stat.hitCount)));
      pnl.addComponent(new Label(MessageFormat.format("Bytes: {0}", stat.hitSize)));
      pnl.addComponent(
          new Label(
              MessageFormat.format(
                  "Distinct IPs: {0}", (int) stat.distinctIpAddress.getEstimate())));

      index++;
    }
  }

  /**
   * Handler to displays alerts status, will limit to the last 4 alert messages
   * @param msg Alert message to display
   */
  public void refreshAlerts(AlertMessage msg) {
    alertMessages.add(msg);
    while (!alertMessages.isEmpty() && alertMessages.size() > 4) {
      alertMessages.pollFirst();
    }

    alerts.removeAllComponents();
    for (AlertMessage alert : alertMessages) {
      TextColor color = TextColor.ANSI.GREEN;
      if (alert.status == AlertMessage.Status.ALERT) {
        color = TextColor.ANSI.RED;
      }
      alerts.addComponent(
          new Label(alert.toHumanString()).setForegroundColor(color).setLabelWidth(1));
    }
  }
}
