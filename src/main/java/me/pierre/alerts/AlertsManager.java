package me.pierre.alerts;

import com.google.common.eventbus.EventBus;
import com.google.common.eventbus.Subscribe;
import me.pierre.model.AlertMessage;
import me.pierre.model.StatisticsMessage;
import me.pierre.statistics.StatisticsAggregator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Deque;
import java.util.LinkedList;

/**
 * Alert State component, consuming {@link StatisticsMessage} to track usage of an interval and
 * yield alerts if necessary
 */
public class AlertsManager {
  private static final Logger logger = LoggerFactory.getLogger(StatisticsAggregator.class);

  // The bus to read StatisticsMessage and send our Alerts to
  private final EventBus eventBus;
  // Buffer of StatisticsMessage for the duration of our alert period span
  private final Deque<StatisticsMessage> statisticsMessages = new LinkedList<>();
  // Count of StatisticsMessage we expect to cover the full alert span
  private final int statisticsInAlertSpan;
  // Threshold of request by second to raise an alert
  private final int alertThresholdBySec;
  // Span duration of the alert period
  private final int alertSpanSec;
  // Track the total count of request in alert span
  private int totalHitsInAlertSpan;
  // Current state of the alert
  private boolean inAlertState;

  public AlertsManager(
      EventBus eventbus, int alertThresholdBySec, int alertSpanSec, int statIntervalSec) {
    this.totalHitsInAlertSpan = 0;
    this.eventBus = eventbus;
    this.alertThresholdBySec = alertThresholdBySec;
    this.alertSpanSec = alertSpanSec;
    // derive the number of StatisticMessage we should get to cover the full alert span
    this.statisticsInAlertSpan = alertSpanSec / statIntervalSec;
  }

  /**
   * Consume a {@link StatisticsMessage} to maintain the alert state
   *
   * @param msg
   */
  private void addStatisticsMessage(StatisticsMessage msg) {
    // Add the newest StatisticsMessage to our buffer
    statisticsMessages.addLast(msg);
    // Add the new request count to our total request tracker count
    totalHitsInAlertSpan += msg.globalStats.hitCount;

    // remove stale StatisticsMessage and reduce the total request tracker count accordingly
    while (!statisticsMessages.isEmpty() && statisticsMessages.size() > statisticsInAlertSpan) {
      StatisticsMessage oldMsg = statisticsMessages.pollFirst();
      totalHitsInAlertSpan -= oldMsg.globalStats.hitCount;
    }

    // Calculate the average hit per second for the alert period
    float hitsPerSecond = (float) totalHitsInAlertSpan / alertSpanSec;

    if (inAlertState) {
      // We are already in an alert state, test if the situation recovered
      if (hitsPerSecond < alertThresholdBySec) {
        inAlertState = false;
        // emit the information about end of alert onto the bus
        eventBus.post(new AlertMessage(AlertMessage.Status.NOALERT, (int) hitsPerSecond));
      }
    } else if (hitsPerSecond >= alertThresholdBySec) {
      // We were not in an alert state, but we violated the threshold, raise the alert.
      inAlertState = true;
      // emit the information about start of alert onto the bus
      eventBus.post(new AlertMessage(AlertMessage.Status.ALERT, (int) hitsPerSecond));
    }
  }

  @Subscribe
  public void handleStatisticsMessage(StatisticsMessage msg) {
    addStatisticsMessage(msg);
  }
}
