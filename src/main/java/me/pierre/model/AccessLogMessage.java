package me.pierre.model;

import java.time.ZonedDateTime;
import java.util.Objects;
import java.util.StringJoiner;

/** Data class to store information about a w3c log line */
public class AccessLogMessage {
  // The IP address of the request
  public String ipAddress;
  // The username of the request
  public String user;
  // The time of the request
  public ZonedDateTime datetime;
  // The verb of the method of the request
  public String verb;
  // The resource accessed by the request.
  public String resource;
  // The procotol used for this request
  public String protocol;
  // The status of the response
  public int status;
  // The size of the response
  public int size;

  public AccessLogMessage(
      String ipAddress,
      String user,
      ZonedDateTime datetime,
      String verb,
      String resource,
      String protocol,
      int status,
      int size) {
    this.ipAddress = ipAddress;
    this.user = user;
    this.datetime = datetime;
    this.verb = verb;
    this.resource = resource;
    this.protocol = protocol;
    this.status = status;
    this.size = size;
  }

  @Override
  public String toString() {
    return new StringJoiner(", ", AccessLogMessage.class.getSimpleName() + "[", "]")
        .add("ipAddress='" + ipAddress + "'")
        .add("user='" + user + "'")
        .add("datetime=" + datetime)
        .add("verb='" + verb + "'")
        .add("resource='" + resource + "'")
        .add("protocol='" + protocol + "'")
        .add("status=" + status)
        .add("size=" + size)
        .toString();
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;
    AccessLogMessage that = (AccessLogMessage) o;
    return status == that.status
        && size == that.size
        && Objects.equals(ipAddress, that.ipAddress)
        && Objects.equals(user, that.user)
        && Objects.equals(datetime, that.datetime)
        && Objects.equals(verb, that.verb)
        && Objects.equals(resource, that.resource)
        && Objects.equals(protocol, that.protocol);
  }

  @Override
  public int hashCode() {
    return Objects.hash(ipAddress, user, datetime, verb, resource, protocol, status, size);
  }
}
