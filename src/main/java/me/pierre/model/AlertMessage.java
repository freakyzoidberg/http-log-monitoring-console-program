package me.pierre.model;

import java.text.MessageFormat;
import java.time.Instant;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.Objects;
import java.util.StringJoiner;

/** Data class for the AlertMessage */
public class AlertMessage {
  // Message pattern for a new alert
  private static String ALERT_MSG =
      "High traffic generated an alert - hits = {0}, triggered at {1}";
  // Message pattern for the end of an alert
  private static String NOALERT_MSG = "Alert recovered at {0}";
  // Formatter for alert timestamp
  private static DateTimeFormatter formatter =
      DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss").withZone(ZoneId.systemDefault());
  // Current state of the alert
  public Status status;
  // Current hit rate of the alert
  public int hitsPerSec;
  // Current alert time
  public Instant alertTime;

  public AlertMessage(Status status, int hitsPerSec) {
    this.status = status;
    this.hitsPerSec = hitsPerSec;
    this.alertTime = Instant.now();
  }

  @Override
  public String toString() {
    return new StringJoiner(", ", AlertMessage.class.getSimpleName() + "[", "]")
        .add("status=" + status)
        .add("hitsPerSec=" + hitsPerSec)
        .toString();
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;
    AlertMessage that = (AlertMessage) o;
    return hitsPerSec == that.hitsPerSec && status == that.status;
  }

  @Override
  public int hashCode() {
    return Objects.hash(status, hitsPerSec);
  }

  public String toHumanString() {
    if (status == Status.ALERT) {
      return MessageFormat.format(ALERT_MSG, hitsPerSec, formatter.format(this.alertTime));
    } else {
      return MessageFormat.format(NOALERT_MSG, formatter.format(this.alertTime));
    }
  };

  public enum Status {
    ALERT,
    NOALERT,
  }
}
