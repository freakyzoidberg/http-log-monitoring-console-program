package me.pierre.model;

import com.yahoo.sketches.hll.HllSketch;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Objects;
import java.util.StringJoiner;

/** Data class for {@link StatisticsMessage} */
public class StatisticsMessage {

  // global Statistics
  public Statistic globalStats = new Statistic();
  // Per section Statistics
  public Map<String, Statistic> sections = new LinkedHashMap<>();
  // Per user Statistics
  public Map<String, Statistic> users = new LinkedHashMap<>();

  @Override
  public String toString() {
    return new StringJoiner(", ", StatisticsMessage.class.getSimpleName() + "[", "]")
        .add("globalStats=" + globalStats)
        .add("sections=" + sections)
        .add("users=" + users)
        .toString();
  }

  /** Data class for {@link Statistic} */
  public static class Statistic {
    // count of requests
    public int hitCount = 0;
    // size of requests
    public int hitSize = 0;

    // count of Sucess responses
    public int status2xx = 0;
    // count of Redirection responses
    public int status3xx = 0;
    // count of Client errors responses
    public int status4xx = 0;
    // count of Server errors responses
    public int status5xx = 0;

    // HyperLogLog sketch to track distinct IP Address
    public HllSketch distinctIpAddress = new HllSketch();

    @Override
    public String toString() {
      return new StringJoiner(", ", Statistic.class.getSimpleName() + "[", "]")
          .add("hitCount=" + hitCount)
          .add("hitSize=" + hitSize)
          .add("status2xx=" + status2xx)
          .add("status3xx=" + status3xx)
          .add("status4xx=" + status4xx)
          .add("status5xx=" + status5xx)
          .add("distinctIpAddress=" + (int) distinctIpAddress.getEstimate())
          .toString();
    }

    @Override
    public boolean equals(Object o) {
      if (this == o) return true;
      if (o == null || getClass() != o.getClass()) return false;
      Statistic statistic = (Statistic) o;
      return hitCount == statistic.hitCount
          && hitSize == statistic.hitSize
          && status2xx == statistic.status2xx
          && status3xx == statistic.status3xx
          && status4xx == statistic.status4xx
          && status5xx == statistic.status5xx
          && Objects.equals(distinctIpAddress, statistic.distinctIpAddress);
    }

    @Override
    public int hashCode() {
      return Objects.hash(
          hitCount, hitSize, status2xx, status3xx, status4xx, status5xx, distinctIpAddress);
    }
  }
}
