package me.pierre.statistics;

import me.pierre.model.AccessLogMessage;
import me.pierre.model.StatisticsMessage;

import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

/** Utility class to process statistics */
public class StatisticsUtil {
  // Regex to extract a section from a resource path.
  private static final Pattern SECTION_PATTERN = Pattern.compile("^(\\/[^\\/]+)\\/\\S*$");

  /**
   * process statistics for the section (if any) of a resource.
   *
   * @param msg the {@link AccessLogMessage} to consume
   * @param statistics the {@link StatisticsMessage} to enrich
   */
  static void processSection(AccessLogMessage msg, StatisticsMessage statistics) {
    if (msg.resource != null) {
      Matcher matcher = SECTION_PATTERN.matcher(msg.resource);
      if (matcher.find()) {
        statistics.sections.compute(matcher.group(1), (s, v) -> processStatistic(msg, v));
      }
    }
  }

  /**
   * process statistics for the user (if any).
   *
   * @param msg the {@link AccessLogMessage} to consume
   * @param statistics the {@link StatisticsMessage} to enrich
   */
  static void processUser(AccessLogMessage msg, StatisticsMessage statistics) {
    if (msg.user != null) {
      statistics.users.compute(msg.user, (s, v) -> processStatistic(msg, v));
    }
  }

  /**
   * Enrich a {@link me.pierre.model.StatisticsMessage.Statistic} with information from an {@link
   * AccessLogMessage}
   *
   * @param msg the {@link AccessLogMessage} to consume
   * @param stats the {@link me.pierre.model.StatisticsMessage.Statistic} to enrich
   * @return
   */
  static StatisticsMessage.Statistic processStatistic(
      AccessLogMessage msg, StatisticsMessage.Statistic stats) {
    // if the Statistic was not created yet, do it now.
    if (stats == null) {
      stats = new StatisticsMessage.Statistic();
    }

    // increment the hit counter
    stats.hitCount++;
    // Add the size of the reponse
    stats.hitSize += msg.size;
    // Add the ipAddress to the HyperLogLog sketch
    stats.distinctIpAddress.update(msg.ipAddress);
    // increment the corresponding status counter
    switch (msg.status / 100) {
      case 2:
        stats.status2xx++;
        break;
      case 3:
        stats.status3xx++;
        break;
      case 4:
        stats.status4xx++;
        break;
      case 5:
        stats.status5xx++;
        break;
      default:
    }

    return stats;
  }

  /**
   * Comparator of Statistics by hitcount
   * @param statA
   * @param statB
   * @return
   */
  public static int compareStatisticByHit(
      Map.Entry<String, StatisticsMessage.Statistic> statA,
      Map.Entry<String, StatisticsMessage.Statistic> statB) {
    return Integer.compare(statA.getValue().hitCount, statB.getValue().hitCount);
  }

  /**
   * Utility to get topN statistics using an arbitrary comparator
   * @param AllHits Map of {@link me.pierre.model.StatisticsMessage.Statistic} by Key
   * @param n number of top elements to return
   * @param compareBy custom comparator for ranking
   * @return top N statistics by key ranked by comparator
   */
  public static Map<String, StatisticsMessage.Statistic> getTopNHits(
      Map<String, StatisticsMessage.Statistic> AllHits,
      int n,
      Comparator<? super Map.Entry<String, StatisticsMessage.Statistic>> compareBy) {
    return AllHits.entrySet().stream()
        .sorted(Collections.reverseOrder(compareBy))
        .limit(n)
        .collect(
            Collectors.toMap(
                Map.Entry::getKey,
                Map.Entry::getValue,
                (u, v) -> {
                  throw new IllegalStateException(String.format("Duplicate key %s", u));
                },
                LinkedHashMap::new));
  }
}
