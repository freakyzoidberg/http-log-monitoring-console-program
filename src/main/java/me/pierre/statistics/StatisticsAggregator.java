package me.pierre.statistics;

import com.google.common.eventbus.EventBus;
import com.google.common.eventbus.Subscribe;
import me.pierre.model.AccessLogMessage;
import me.pierre.model.StatisticsMessage;

import java.time.Instant;
import java.util.Deque;
import java.util.TimerTask;
import java.util.concurrent.ConcurrentLinkedDeque;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import static me.pierre.statistics.StatisticsUtil.*;

/** Periodical aggregator of {@link AccessLogMessage} into {@link StatisticsMessage} */
public class StatisticsAggregator {
  // Buffer of AccessLogMessage to process in the next batch.
  private final Deque<AccessLogMessage> accessLogMessages = new ConcurrentLinkedDeque<>();
  // Our bus where to subscribe to AccessLogMessage and post StatisticsMessage
  private final EventBus eventBus;
  // The maximum timestamp we should consider for aggregating the current batch of AccessLogMessage
  private Instant maxTimestamp;

  /**
   * Construtor with scheduling of aggregation task enabled
   *
   * @param eventBus the event bus to read {@link AccessLogMessage} from and post {@link
   *     StatisticsMessage} to
   * @param statIntervalSec the interval rate for generating {@link StatisticsMessage}
   */
  public StatisticsAggregator(EventBus eventBus, int statIntervalSec) {
    this(eventBus, statIntervalSec, true);
  }

  /**
   * Constructor with optional scheduling of aggregation task (used for test)
   *
   * @param eventBus the event bus to read {@link AccessLogMessage} from and post {@link
   *     StatisticsMessage} to
   * @param statIntervalSec the interval rate for generating {@link StatisticsMessage}
   * @param withScheduling flag to enable/disable aggregation task scheduling
   */
  public StatisticsAggregator(EventBus eventBus, int statIntervalSec, boolean withScheduling) {
    this.eventBus = eventBus;
    // Use the current time plus the statIntervalSec as our new upper bound
    this.maxTimestamp = Instant.now().plusSeconds(statIntervalSec);

    if (withScheduling) {
      // Periodically schedule (at statIntervalSec) the aggregation task.
      TimerTask aggregationTask =
          new TimerTask() {
            public void run() {
              // process the AccessLogMessage in the buffer and yield the StatisticsMessage on the
              // bus
              eventBus.post(aggregateStatistics());
              // update the maxTimestamp for our next batch
              maxTimestamp = maxTimestamp.plusSeconds(statIntervalSec);
            }
          };
      ScheduledExecutorService executor = Executors.newSingleThreadScheduledExecutor();
      executor.scheduleAtFixedRate(
          aggregationTask, statIntervalSec, statIntervalSec, TimeUnit.SECONDS);
    }
  }

  /**
   * Aggregate all {@link AccessLogMessage} in our buffer for the duration of the current batch
   * generate various stats like global request counts / size / distinct IpAddresses and per section
   * and user
   *
   * @return a {@link StatisticsMessage} for the period.
   */
  protected StatisticsMessage aggregateStatistics() {
    StatisticsMessage statisticsMessage = new StatisticsMessage();

    // consume the last message from our queue (the oldest)
    AccessLogMessage msg = accessLogMessages.pollLast();

    // iterate until the queue is empty
    while (msg != null) {
      // if the message is too recent, put it back at the end of our queue and bail out
      if (msg.datetime.toInstant().isAfter(maxTimestamp)) {
        accessLogMessages.addLast(msg);
        break;
      }

      // process the message into the global statistics
      processStatistic(msg, statisticsMessage.globalStats);
      // process the message into the sections statistics
      processSection(msg, statisticsMessage);
      // process the message into the user statistics
      processUser(msg, statisticsMessage);

      msg = accessLogMessages.pollLast();
    }

    return statisticsMessage;
  }

  /**
   * Add the {@link AccessLogMessage} to our buffer pending aggregation
   *
   * @param msg an {@link AccessLogMessage} to process
   */
  @Subscribe
  public void handleAccessLogMessage(AccessLogMessage msg) {
    accessLogMessages.addFirst(msg);
  }
}
