
# HTTP log monitoring console program

Console application consuming w3c formatter HTTP access log with statistics and alerting.

## Getting started

### Prerequisites

Requires Java 8, maven for build and optionally Docker

### Usage

Start the program

```
./httpMonitor.sh
```

For usage and available options 

```bash
usage: ./httpMonitor.sh [-a <arg>] [-c] [-f <arg>] [-h] [-s <arg>] [-t
       <arg>]
 -a,--alert-span <arg>        Alerts span (default: 120sec)
 -c,--console                 Disable GUI - use console instead
 -f,--file-access-log <arg>   HTTP log file (default: /tmp/access.log)
 -h,--help                    Show this usage
 -s,--stat-interval <arg>     Statistics interval (default: 10sec)
 -t,--alert-threshold <arg>   Alerts threshold (default: 10/sec)```
```

### Docker
You can build a docker image to run the application in a container

Make sure to configure docker with the correct COLUMNS/LINES environment from your term.

```aidl
docker build -t http-monitor .

docker run -e COLUMNS=$COLUMNS -e LINES=$LINES -it http-monitor
```

## Output

The application comes with a NCurse like GUI (default) and a simpler log to console approach (with flag --console)

### Full GUI

![GUI](datadog.gif)


### Console log

```
2020-03-05 21:52:30,544 INFO  - StatisticsMessage[globalStats=Statistic[hitCount=297, hitSize=1617714, status2xx=216, status3xx=0, status4xx=81, status5xx=0, distinctIpAddress=297], sections={/sectionB=Statistic[hitCount=120, hitSize=656246, status2xx=88, status3xx=0, status4xx=32, status5xx=0, distinctIpAddress=120], /secTionA=Statistic[hitCount=89, hitSize=487346, status2xx=62, status3xx=0, status4xx=27, status5xx=0, distinctIpAddress=89]}, users={frank=Statistic[hitCount=163, hitSize=929040, status2xx=120, status3xx=0, status4xx=43, status5xx=0, distinctIpAddress=163], james=Statistic[hitCount=134, hitSize=688674, status2xx=96, status3xx=0, status4xx=38, status5xx=0, distinctIpAddress=134]}]
2020-03-05 21:52:40,516 INFO  - StatisticsMessage[globalStats=Statistic[hitCount=292, hitSize=1670561, status2xx=209, status3xx=0, status4xx=83, status5xx=0, distinctIpAddress=292], sections={/secTionA=Statistic[hitCount=84, hitSize=485450, status2xx=62, status3xx=0, status4xx=22, status5xx=0, distinctIpAddress=84], /sectionB=Statistic[hitCount=130, hitSize=755428, status2xx=89, status3xx=0, status4xx=41, status5xx=0, distinctIpAddress=130]}, users={frank=Statistic[hitCount=138, hitSize=805371, status2xx=99, status3xx=0, status4xx=39, status5xx=0, distinctIpAddress=138], james=Statistic[hitCount=154, hitSize=865190, status2xx=110, status3xx=0, status4xx=44, status5xx=0, distinctIpAddress=154]}]
2020-03-05 21:52:50,514 INFO  - StatisticsMessage[globalStats=Statistic[hitCount=298, hitSize=1655035, status2xx=204, status3xx=0, status4xx=94, status5xx=0, distinctIpAddress=298], sections={/sectionB=Statistic[hitCount=116, hitSize=653454, status2xx=79, status3xx=0, status4xx=37, status5xx=0, distinctIpAddress=116], /secTionA=Statistic[hitCount=91, hitSize=488419, status2xx=60, status3xx=0, status4xx=31, status5xx=0, distinctIpAddress=91]}, users={james=Statistic[hitCount=143, hitSize=782854, status2xx=99, status3xx=0, status4xx=44, status5xx=0, distinctIpAddress=143], frank=Statistic[hitCount=155, hitSize=872181, status2xx=105, status3xx=0, status4xx=50, status5xx=0, distinctIpAddress=155]}]
2020-03-05 21:53:00,514 INFO  - StatisticsMessage[globalStats=Statistic[hitCount=282, hitSize=1606474, status2xx=199, status3xx=0, status4xx=83, status5xx=0, distinctIpAddress=282], sections={/sectionB=Statistic[hitCount=98, hitSize=557332, status2xx=65, status3xx=0, status4xx=33, status5xx=0, distinctIpAddress=98], /secTionA=Statistic[hitCount=89, hitSize=508981, status2xx=63, status3xx=0, status4xx=26, status5xx=0, distinctIpAddress=89]}, users={james=Statistic[hitCount=139, hitSize=817046, status2xx=92, status3xx=0, status4xx=47, status5xx=0, distinctIpAddress=139], frank=Statistic[hitCount=143, hitSize=789428, status2xx=107, status3xx=0, status4xx=36, status5xx=0, distinctIpAddress=143]}]
2020-03-05 21:53:10,516 INFO  - StatisticsMessage[globalStats=Statistic[hitCount=291, hitSize=1603545, status2xx=213, status3xx=0, status4xx=78, status5xx=0, distinctIpAddress=291], sections={/sectionB=Statistic[hitCount=123, hitSize=686196, status2xx=89, status3xx=0, status4xx=34, status5xx=0, distinctIpAddress=123], /secTionA=Statistic[hitCount=96, hitSize=527175, status2xx=67, status3xx=0, status4xx=29, status5xx=0, distinctIpAddress=96]}, users={frank=Statistic[hitCount=137, hitSize=750062, status2xx=97, status3xx=0, status4xx=40, status5xx=0, distinctIpAddress=137], james=Statistic[hitCount=154, hitSize=853483, status2xx=116, status3xx=0, status4xx=38, status5xx=0, distinctIpAddress=154]}]
2020-03-05 21:53:10,519 WARN  - High traffic generated an alert - hits = 12, triggered at 2020-03-05T20:53:10.517307Z
```



## Design & Architecture

This application is designed as a set of componets loosely coupled by an Event Bus.

Each component is responsible a specific task and their output can serve as the input of another component.

There is currently 4 components on this bus an new components can easily be added to consume existing messages

### Components
    
#### AccessLogProducer
This component is responsible to watch and tail a http log files while generating AccessLogMessages onto the bus

the AccessLogMessages contains the following information

    - ipAddress # The IP address of the request
    - user # the username of the request
    - datetime # TZ aware datetime of the request
    - verb # the verb of the request
    - resource # the resource accessed by the request
    - protocol # the protocol used for the request
    - status # the status of the reponse
    - size # the number of bytes of the response


#### StatisticsAggregator
Periodically aggregating AccessLogMessages into StatisticsMessages

Statistics are generated globally, per Section and per User.

Each entity will provide the following statistics

    - hitCount (int) # number of request in the period
    - hitSize (int) # sum of bytes of response in the period
    
    - status2xx (int) # number of Success response
    - status3xx (int) # number of Redirection Response
    - status4xx (int) # number of Client error Response
    - status5xx (int) # number of Server error Response
    
    - distinctIpAddress (HyperLogLog) # cardinality estimator for IP addresses

Note the use of HyperLogLog to track the distinct IP address, the implementation used will be exact up until 2048 distinct element then will be probabilistic with a relative standard error of 0.016
The main advantage of using HLL for high cardinality is the space reduction it provide while keeping the possibility to merge different sketch to estimate the cardinality of the union of sets. 

#### AlertsManager
Consuming StatisticsMessages and maintaining a state of the current alerts.

It can yield AlertMessage when a new alert begin or end with the hitrate at that moment for the requested alerting duration 


#### ConsoleGui
Consuming StatisticsMessages and AlertMessages for rendering.

### Event Flow

```aidl

                                                              +-AlertMessage-+
                                                              |              v
+-------------------+  +-------------------+  +---------------+---+  +-------------------+
|                   |  |                   |  |                   |  |                   |
|     AccessLog     |  |     Statistics    |  |      Alerts       |  |      Console      |
|     Producer      |  |     Aggregator    |  |      Manager      |  |        GUI        |
|                   |  |                   |  |                   |  |                   |
+-----+-------------+  +-+------------+--+-+  +-+-----------------+  +---+---------------+
      |                  ^            |  |      ^                        ^
      +-AccessLogMessage-+            |  +------+                        |
                                      | StatisticsMessage                |
                                      +----------------------------------+

```

# Improvement on application design

* The models should be interfaceable allowing for extension of future components.
* The Statistics aggregator could aggregate as soon as AccessLogMessage are available rather than at the end of the statistics period.
* Adding a component allowing to forward the statistics to an external system would be interesting (REST?, Websocket? Raw stream?)
* Code coverage could be improved


## Building & Testing


### Building
 
```
mvn clean package 
```

### Running dev build
```
java -jar ./target/http-monitor.jar
```

### Testing

```
mvn test
```