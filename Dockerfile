FROM anapsix/alpine-java:latest
RUN touch /var/log/access.log
COPY httpMonitor.sh http-monitor.jar /
ENTRYPOINT [ "java", "-jar", "http-monitor.jar"]